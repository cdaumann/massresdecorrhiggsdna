import os 
import awkward as ak
import numpy as np
from coffea.nanoevents import NanoEventsFactory, BaseSchema, NanoAODSchema
import boost_histogram as bh 
import matplotlib.pyplot as plt 
import mplhep, hist
plt.style.use([mplhep.style.CMS])
import pandas as pd
import scipy

def weighted_median_(values, weights):
    i = np.argsort(values)
    c = np.cumsum(weights[i])
    return values[i[np.searchsorted(c, 0.5 * c[-1])]]

#def weighted_quantiles_interpolate(values, weights, quantiles=0.5):
#    i = np.argsort(values)
#    c = np.cumsum(weights[i])
#    q = np.searchsorted(c, quantiles * c[-1])
#    return np.where(c[q]/c[-1] == quantiles, 0.5 * (values[i[q]] + values[i[q+1]]), values[i[q]])

def weighted_quantiles_interpolate(values, weights, quantiles=0.5):
    i = np.argsort(values)
    c = np.cumsum(weights[i])
    return values[i[np.searchsorted(c, np.array(quantiles) * c[-1])]]

def calculate_bins_position(mass, weights):

    #This code below is responsable for separate the mass events in 8 bins of equal number of events
    sorted_data = np.sort(mass) #This sorts the mass in increasing order

    # Calculate the number of elements in each bin
    num_elements = len(sorted_data) // 8

    # Initialize a list to store the bins
    bins,x_bins = [],[]
    x_bins.append(100)

    # Create 8 bins with the same number of events
    for i in range(0, len(sorted_data), num_elements):
        bin_data = sorted_data[i:i + num_elements]
        bins.append(bin_data)
        x_bins.append( float(np.max(bin_data)) )

    x_bins.pop()

    return x_bins

def plot_sigma_over_m_profile(position, mean_value, mean_value_decorr,mean_value_10,mean_value_decorr_10,mean_value_90,mean_value_decorr_90):
    

    plt.close()

    #10% quantile
    plt.plot( position, mean_value_decorr_10 ,linestyle='dashed', linewidth = 4, color = 'orange' )
    plt.plot( position, mean_value_10        ,linestyle='dashed', linewidth = 4, color = 'blue'   )

    #50 quantile
    plt.plot( position, mean_value_decorr , linewidth = 4, color = 'orange', label =   "decorrelated")
    plt.plot( position, mean_value , linewidth = 4,        color = 'blue'    ,label =  "not decorrelated")

    #90% quantile
    plt.plot( position, mean_value_decorr_90 ,linestyle='dashed', linewidth = 4, color = 'orange' )
    plt.plot( position, mean_value_90        ,linestyle='dashed', linewidth = 4, color = 'blue'   )

    x = [95,175]
    y = [0.0104,0.0104]
    #plt.plot(x, y, linewidth = 3, linestyle='--'color = 'red')
    plt.ylim( 0.003, 0.022 )
    plt.xlabel( 'Diphoton Mass' )
    plt.ylabel( 'Sigma_over_m' )
    plt.tight_layout()
    plt.legend(  )
    plt.savefig('sucess_decorr.png')
    plt.savefig('sucess_decorr.pdf')

def plot_CDFS(mass, sigma_over_m,sigma_over_m_decorr,mc_weights):
    #First lets separate the mass and the uncertantities in three bins (100:100,5 ; 125:125,5 : )
    mass_mask_1 =  np.logical_and( mass > 100 , mass < 102 ) 
    mass_mask_2 =  np.logical_and( mass > 124 , mass < 126 ) 
    mass_mask_3 =  np.logical_and( mass > 164 , mass < 166 )

    great_mask = [mass_mask_1, mass_mask_2, mass_mask_3]
    great_CDF  = []

    #First, the plot of the sigma_over_m correlated CDF in the three bins
    for i in range( 3 ):
        
        val = sigma_over_m[ great_mask[i] ]

        dBins = dBins=np.linspace(0.,0.5,1001)

        hist, _ = np.histogram(val, weights=mc_weights[ great_mask[i] ], bins=dBins)
        rightEdge = dBins[1:]
        bCum = np.cumsum(hist)
        bCum[bCum < 0.] = 0
        bCum /= bCum.max()
        cdfBinned = np.vstack((bCum,rightEdge))

        great_CDF.append(cdfBinned)

    #Ploting:
    plt.close()
    plt.plot( great_CDF[0][1], great_CDF[0][0], color='blue'  , alpha=0.7, linewidth = 2,label = ' 100 < m < 102 ')
    plt.plot( great_CDF[1][1], great_CDF[1][0], color='red'   , alpha=0.7, linewidth = 2,label = ' 124 < m < 126 ')
    plt.plot( great_CDF[2][1], great_CDF[2][0], color='purple', alpha=0.7, linewidth = 2,label = ' 164 < m < 166 ')
    plt.legend()
    plt.xlim(0,0.035)
    plt.ylabel( 'CDF' )
    plt.xlabel('sigma_over_m')
    plt.savefig( 'r_CDF.png' )

    #Now, the same plot but using the decorrelated sigma_over_m

    great_CDF  = []

    #First, the plot of the sigma_over_m correlated CDF in the three bins
    for i in range( 3 ):
        
        val = sigma_over_m_decorr[ great_mask[i] ]

        dBins = dBins=np.linspace(0.,0.5,1001)

        hist, _ = np.histogram(val, weights=mc_weights[ great_mask[i] ], bins=dBins)
        rightEdge = dBins[1:]
        bCum = np.cumsum(hist)
        bCum[bCum < 0.] = 0
        bCum /= bCum.max()
        cdfBinned = np.vstack((bCum,rightEdge))

        great_CDF.append(cdfBinned)

    #Ploting:
    plt.close()
    plt.plot( great_CDF[0][1], great_CDF[0][0], color='blue'  , alpha=0.7, linewidth = 2,label = ' 100 < m < 100.5 ')
    plt.plot( great_CDF[1][1], great_CDF[1][0], color='red'   , alpha=0.7, linewidth = 2,label = ' 125 < m < 125.5 ')
    plt.plot( great_CDF[2][1], great_CDF[2][0], color='purple', alpha=0.7, linewidth = 2,label = ' 160 < m < 160.5 ')
    plt.legend()
    plt.xlim(0,0.035)
    plt.ylabel( 'CDF' )
    plt.xlabel('sigma_over_m_decorrelated')
    plt.savefig( 'r_CDF_decorr.png' )


    return None

#ploting algorithm to plot the results of mass decorrelator algorithm
def main():

    #Reading the .parquet file contatinng the original trees + the decorrelated mass resolution
    vector = ak.from_parquet("modified_file.parquet")

    #Reading the colums from the ak object
    sigma_m_over_m = vector["sigma_m_over_m"]
    sigma_m_over_m_decorr = vector["sigma_m_over_m_decorr"]
    mass = vector["mass"]
    mc_weights = vector["weight"]

    #Performing some cuts on the mass, to stay in the 100 < m < 180 analysis range
    mask_mass =  np.logical_and( mass >= 100, mass <= 179)
    mass, sigma_m_over_m,sigma_m_over_m_decorr,mc_weights = mass[mask_mass],sigma_m_over_m[mask_mass], sigma_m_over_m_decorr[mask_mass], mc_weights[mask_mass]

    x_bins = calculate_bins_position(mass,vector["weight"]) #This gives the mass values of separating the mass distirbution in 8 bins of equal number of events

    #This loop down here calculate the median of the sigma_over_m for the mass events in each of the 8 bins calculated above
    position, mean_value,mean_value_decorr,weighted_mean_value_decorr = [],[],[],[]

    mean_value_10,mean_value_decorr_10 = [],[]
    mean_value_90,mean_value_decorr_90 = [],[]
    for i in range( len(x_bins)-1 ):
        #mass_min,mass_max = np.min( x_bins[i] ), np.max( x_bins[i+1] )
        mass_window = np.logical_and(  mass >=  x_bins[i], mass <=  x_bins[i+1])
        sigma_m_over_m_decorr_inside_window   = sigma_m_over_m_decorr[mass_window]
        sigma_m_over_m_inside_window          = sigma_m_over_m[mass_window]
        w_inside_window                       = mc_weights[mass_window]

        position.append(   float( (x_bins[i]+x_bins[i+1])/2 ))
        mean_value.append(  weighted_quantiles_interpolate(sigma_m_over_m_inside_window,w_inside_window) )
        mean_value_decorr.append( weighted_quantiles_interpolate(sigma_m_over_m_decorr_inside_window,w_inside_window) )

        mean_value_10.append(  weighted_quantiles_interpolate(sigma_m_over_m_inside_window,w_inside_window,quantiles=0.10) )
        mean_value_decorr_10.append( weighted_quantiles_interpolate(sigma_m_over_m_decorr_inside_window,w_inside_window,quantiles=0.10) )

        mean_value_90.append(  weighted_quantiles_interpolate(sigma_m_over_m_inside_window,w_inside_window,quantiles=0.90) )
        mean_value_decorr_90.append( weighted_quantiles_interpolate(sigma_m_over_m_decorr_inside_window,w_inside_window,quantiles=0.90) )



    plot_sigma_over_m_profile(position, mean_value, mean_value_decorr,mean_value_10,mean_value_decorr_10,mean_value_90,mean_value_decorr_90)

    plot_CDFS(mass, sigma_m_over_m,sigma_m_over_m_decorr, vector["weight"])

if __name__ == "__main__":
    main()

exit()

#End of the important part!

mass_res = vector["sigma_m_over_m"]
mass_ress_deco = vector["sigma_m_over_m_decorr"]
mass = vector["mass"]
weights_o = vector["weight"]


plt.close()
data1 = mass_res[ np.logical_and( mass > 123 , mass < 127 ) ]
plt.hist(data1, range = ( 0.0025, 0.02 ) ,bins=40, density=True, color='blue', alpha=0.7, edgecolor='black')
plt.savefig( 'reference.png' )

plt.close()
data1 = mass_res[ np.logical_and( mass > 110 , mass < 114) ]
plt.hist(data1, range = ( 0.0025,0.02 ) ,bins=40, density=True, color='blue', alpha=0.7, edgecolor='black')
plt.savefig( 'reference_low.png' )

#ploting the cff

mask = np.logical_and( mass > 100 , mass < 100.5)
val = mass_res[mask]
weights = weights_o[mask]

dBins = dBins=np.linspace(0.,0.5,1001)

hist, _ = np.histogram(val, weights=weights, bins=dBins)
rightEdge = dBins[1:]
bCum = np.cumsum(hist)
bCum[bCum < 0.] = 0
bCum /= bCum.max()
cdfBinned = np.vstack((bCum,rightEdge))

#Again

mask = np.logical_and( mass > 160 , mass < 160.5)
val = mass_res[mask]
weights = weights_o[mask]

hist, _ = np.histogram(val, weights=weights, bins=dBins)
rightEdge = dBins[1:]
bCum = np.cumsum(hist)
bCum[bCum < 0.] = 0
bCum /= bCum.max()
cdfBinned_1 = np.vstack((bCum,rightEdge))

mask = np.logical_and( mass > 125 , mass < 125.5)
val = mass_res[mask]
weights = weights_o[mask]

hist, _ = np.histogram(val, weights=weights, bins=dBins)
rightEdge = dBins[1:]
bCum = np.cumsum(hist)
bCum[bCum < 0.] = 0
bCum /= bCum.max()
cdfBinned_2 = np.vstack((bCum,rightEdge))


plt.close()
data1 = mass_res[ np.logical_and( mass > 110 , mass < 114) ]
plt.plot(cdfBinned[1],cdfBinned[0], color='blue', alpha=0.7, label = ' 100 < m < 100.5 ')
plt.plot(cdfBinned_1[1],cdfBinned_1[0], color='red', alpha=0.7, label = '160 < m < 160.5 ')
plt.plot(cdfBinned_2[1],cdfBinned_2[0], color='purple', alpha=0.7, label = ' 125 < m < 125.5 ')
plt.legend()
plt.xlim(0,0.035)
plt.savefig( 'r_CDF.png' )

exit()

###########
#Now I will plot the sigma_over_m for 3 diferent mass ranges!
# Create a figure with three subplots arranged side-by-side

data1 = mass_res[ np.logical_and( mass > 100 , mass < 110 ) ]
data2 = mass_res[ np.logical_and( mass > 120 , mass < 130 ) ]
data3 = mass_res[ np.logical_and( mass > 160 , mass < 170 ) ]

fig, axes = plt.subplots(1, 3, figsize=(18, 6), sharey=True)  # 1 row and 3 columns

# Plot histograms on each subplot
axes[0].hist(data1, range = ( 0,0.015 ) ,bins=60, density=True, color='blue', alpha=0.7, edgecolor='black')
axes[0].set_title('Histogram 1')

axes[1].hist(data2, range = ( 0,0.015 ) ,bins=60, density=True, color='green', alpha=0.7, edgecolor='black')
axes[1].set_title('Histogram 2')

axes[2].hist(data3,range = ( 0,0.015) , bins=60, density=True, color='red', alpha=0.7, edgecolor='black')
axes[2].set_title('Histogram 3')

# Add labels and a common y-axis label

axes[0].set_xlabel('Value')
axes[0].set_ylabel('Frequency')

for ax in axes:
    ax.set_xlim(0,0.015)
    ax.set_yscale('log')

# Adjust spacing between subplots
plt.tight_layout()

# Show the plot
plt.savefig('sigma_mass_ranges.png')

#The abover plot dont really work =p

data1 = mass_ress_deco[ np.logical_and( mass > 100 , mass < 110 ) ]
data2 = mass_ress_deco[ np.logical_and( mass > 120 , mass < 130 ) ]
data3 = mass_ress_deco[ np.logical_and( mass > 160 , mass < 170 ) ]

fig, axes = plt.subplots(1, 3, figsize=(18, 6), sharey=True)  # 1 row and 3 columns

# Plot histograms on each subplot
axes[0].hist(data1, range = ( 0,0.05 ) ,bins=60, density=True, color='blue', alpha=0.7, edgecolor='black')
axes[0].set_title('Histogram 1')

axes[1].hist(data2, range = ( 0,0.05 ) ,bins=60, density=True, color='green', alpha=0.7, edgecolor='black')
axes[1].set_title('Histogram 2')

axes[2].hist(data3,range = ( 0,0.05) , bins=60, density=True, color='red', alpha=0.7, edgecolor='black')
axes[2].set_title('Histogram 3')

# Add labels and a common y-axis label

axes[0].set_xlabel('Value')
axes[0].set_ylabel('Frequency')

for ax in axes:
    ax.set_xlim(0,0.05)
    ax.set_yscale('log')

# Adjust spacing between subplots
plt.tight_layout()

# Show the plot
plt.savefig('sigma_mass_ranges_deco.png')


#The plots are ploted ok! But meh .... 


exit()

hist_real = hist.Hist(hist.axis.Regular(30, 115, 130))
hist_real.fill(mass  )

fig, ax = plt.subplots(figsize=(8,7))

mplhep.histplot(
        [hist_real],
        label = ["Photons"],
        yerr=True,
        density = True,
        linewidth=3
    )

plt.xlabel("Mass dist", fontsize=22)
plt.ylabel("Density", fontsize=22)

mplhep.cms.label(data=False, ax=ax, loc=0, label="Private Work", com=13.6, fontsize=22)

ax.set_ylim(0, 1.20*ax.get_ylim()[1])

plt.legend(fontsize=22, loc="upper right")
fig.tight_layout()

fig.savefig("mass_dist.pdf")

#Mass unc before decorrelation --- 
mass_res_hist = hist.Hist(hist.axis.Regular(30, 0.01, 0.1))
mass_res = vector["sigma_m_over_m"]

plt.close()

fig, ax = plt.subplots(figsize=(8,7))
plt.hist( mass_res, range = (0,0.05), bins = 50, label = 'Mass resolution/mass' )
plt.savefig('mass_resolution.png')
plt.close()

#Mass resolution over a range
mass_res = mass_res[  np.logical_and( mass > 125, mass < 130 )  ]

fig, ax = plt.subplots(figsize=(8,7))
plt.hist( mass_res, range = (0,0.05), bins = 50, label = 'Mass resolution/mass' )
plt.savefig('mass_resolution_range.png')
plt.close()

#now decorrelated mass
mass_res_hist = hist.Hist(hist.axis.Regular(30, 0.01, 0.03))
mass_res = vector["sigma_m_over_m_decorr"]

fig, ax = plt.subplots(figsize=(8,7))
plt.hist( mass_res, range = (0,0.025), bins = 500, label = 'Mass resolution decorr/mass' )
plt.savefig('mass_resolution__decorr.png')
plt.close()